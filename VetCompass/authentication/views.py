from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate


def index(request):

    # check request method for page redirect
    if request.method == "GET":
        template = loader.get_template('authentication/index.html')
        context = {}
        return HttpResponse(template.render(context, request))
    else:
        return verify(request)


# verify user account and password
def verify(request):

    username = request.POST.get('username','')
    password = request.POST.get('password','')

    # check username and password match in database
    user = authenticate(username=username, password=password)

    if user is not None:
        return HttpResponse("correct username or password")
    else:
        return HttpResponse("wrong username and password",status=401)


