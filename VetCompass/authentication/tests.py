from django.test import TestCase
from django.test import Client
from django.urls import reverse
from django.contrib.auth.models import User
# Create your tests here.


class SimpleLoginTest(TestCase):
    def setUp(self):

        # initialise a testing username account
        test_user_norm = User.objects.create_user('josh', 'lennon@thebeatles.com', 'joshpassword')
        test_user_norm.save()

    # test login with invalid account
    def test_invalidAccountLogin(self):
        c = Client()

        # correct username and wrong password
        response = c.post('/login/', {'username': 'josh', 'password': 'joshpasswords'})
        self.assertEqual(response.status_code, 401)

        # wrong username and correct password
        response = c.post('/login/', {'username': 'jos', 'password': 'joshpassword'})
        self.assertEqual(response.status_code, 401)


    def test_validAccountLogin(self):
        c = Client()
        response = c.post('/login/', {'username': 'josh', 'password': 'joshpassword'})
        self.assertEqual(response.status_code, 200)
