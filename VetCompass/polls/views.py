from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.template import loader
from .models import Question
from django.http import Http404




def index(request):
    # request
    # user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
    # user.last_name = 'Lennon'
    # user.save()
    # print(type(request))
    # return HttpResponse("Hello, world. You're at the polls indexkds.", request.body)

    # if request.method == 'GET':
    #     return HttpResponse("Hello, world. You're at the polls indexkds. GET")
    # else:
    #     return HttpResponse("Hello, world. You're at the polls indexkds.", "POST")

    latest_question_list = Question.objects.order_by('-pub_date')[:5]

    # output = ', '.join([q.question_text for q in latest_question_list])

    # template = loader.get_template('polls/index.html')
    #
    # context = {
    #     'latest_question_list': latest_question_list,
    # }

    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)

    # return HttpResponse(template.render(context, request))



def detail(request, question_id):
    # try:
    #     question = Question.objects.get(pk=question_id)
    # except Question.DoesNotExist:
    #     raise Http404("Question does not exist")
    # return render(request, 'polls/detail.html', {'question': question})

    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)